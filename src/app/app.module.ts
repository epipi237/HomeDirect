import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import {HttpClientModule} from '@angular/common/http';

import { ROUTING } from "./app.routing";
//import { HttpModule }    from '@angular/http';

import { UserComponent } from "./route/users/user.component";
import { UserService } from "./route/users/user.service";
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AccountComponent } from './account/account.component';
import { TransferComponent } from './transfer/transfer.component';
import { ProfileComponent } from './profile/profile.component';
import { StatementComponent } from './statement/statement.component';
import { BillComponent } from './bill/bill.component';
import { AdminComponent } from './admin/admin.component';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        HttpModule,
        ROUTING
    ],
    declarations: [
        AppComponent,
        UserComponent,
        LoginComponent,
        RegisterComponent,
        AccountComponent,
        TransferComponent,
        ProfileComponent,
        StatementComponent,
        BillComponent,
        AdminComponent
    ],
    providers:[
        UserService
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {}