import { RouterModule, Routes } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";

import { UserComponent } from "./route/users/user.component";
import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./register/register.component";
import {AccountComponent} from "./account/account.component";
import {StatementComponent} from "./statement/statement.component";
import {TransferComponent} from "./transfer/transfer.component";
import {BillComponent} from "./bill/bill.component";
import { ProfileComponent } from "./profile/profile.component";
import {AdminComponent}  from "./admin/admin.component";


const APP_ROUTES: Routes = [
    {
        path: "user",
        component: UserComponent
    },
    {
        path: "register",
        component: RegisterComponent
    },
     {
        path: "statement",
        component: StatementComponent
    },
    {
        path: "transfers",
        component: TransferComponent
    },
    {
        path: "inquiry",
        component: ProfileComponent
    },
    {
        path: "bills",
        component: BillComponent
    },
    {
        path: "account",
        component: AccountComponent
    },
    {
        path: "admin",
        component: AdminComponent
    },
    {
        path: "login",
        component: LoginComponent
    }
];

export const ROUTING: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);